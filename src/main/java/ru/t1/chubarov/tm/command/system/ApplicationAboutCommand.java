package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeni Chubarov");
        System.out.println("e-mail: echubarov@t1-consulting.ru");
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show about program.";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

}
