package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IRepository;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    M add(@Nullable M model) throws AbstractException;

    @NotNull
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator<M> comparator);

    void removeAll();

    @NotNull
    M remove(@Nullable M model) throws AbstractException;

    @NotNull
    M findOneById(@NotNull String id) throws AbstractException;

    @NotNull
    M findOneByIndex(@NotNull Integer index) throws AbstractException;

    boolean existsById(@NotNull String id);

}
