package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @Nullable
    IProjectService getProjectService();

    @Nullable
    ITaskService getTaskService();

    @Nullable
    IProjectTaskService getProjectTaskService();

    @Nullable
    ICommandService getCommandService();

    @Nullable
    ILoggerService getLoggerService();

    @Nullable
    abstract IUserService getUserService();

    @Nullable
    IAuthService getAuthService();

}
