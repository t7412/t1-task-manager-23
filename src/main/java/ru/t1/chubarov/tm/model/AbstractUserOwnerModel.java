package ru.t1.chubarov.tm.model;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    private String userid;

    @Nullable
    public String getUserid() {
        return userid;
    }

    public void setUserid(@Nullable String userid) {
        this.userid = userid;
    }

}
