package ru.t1.chubarov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.command.project.*;
import ru.t1.chubarov.tm.command.system.*;
import ru.t1.chubarov.tm.command.task.*;
import ru.t1.chubarov.tm.command.user.*;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.chubarov.tm.exception.system.CommandNotSupportedException;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.CommandRepository;
import ru.t1.chubarov.tm.repository.ProjectRepository;
import ru.t1.chubarov.tm.repository.TaskRepository;
import ru.t1.chubarov.tm.repository.UserRepository;
import ru.t1.chubarov.tm.service.*;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository,taskRepository,projectRepository);
    @NotNull
    private final IAuthService authService = new AuthService(userService);


    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }


    private void initDemoData() throws AbstractException {

        @NotNull final User test = userService.create("test", "test", "test@emal.ru");
        @NotNull final User user = userService.create("user", "user", "user@emal.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("ALFA PROJECT", Status.NOT_STARTED));
        projectService.add(new Project("BETA PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("DELTA PROJECT", Status.COMPLETED));
        taskService.create(test.getId(), "FIRST TASK", "FIRST DESCRIPTION");
        taskService.create(test.getId(), "SECOND TASK", "SECOND DESCRIPTION");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    private void regestry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) throws AbstractException {
        if (processArguments(args)) System.exit(0);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private boolean processArguments(@Nullable final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    private void processArgument(@Nullable final String arg) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    {
        regestry(new ApplicationAboutCommand());
        regestry(new ApplicationExitCommand());
        regestry(new ApplicationHelpCommand());
        regestry(new ApplicationVersionCommand());
        regestry(new ArgumentListCommand());
        regestry(new CommandListCommand());
        regestry(new SystemInfoCommand());

        regestry(new ProjectShowCommand());
        regestry(new ProjectChangeStatusByIdCommand());
        regestry(new ProjectChangeStatusByIndexCommand());
        regestry(new ProjectCompleteByIdCommand());
        regestry(new ProjectCompleteByIndexCommand());
        regestry(new ProjectCreateCommand());
        regestry(new ProjectRemoveByIdCommand());
        regestry(new ProjectRemoveByIndexCommand());
        regestry(new ProjectsClearCommand());
        regestry(new ProjectShowByIdCommand());
        regestry(new ProjectShowByIndexCommand());
        regestry(new ProjectStartByIdCommand());
        regestry(new ProjectStartByIndexCommand());
        regestry(new ProjectUpdateByIdCommand());
        regestry(new ProjectUpdateByIndexCommand());
        regestry(new ProjectTaskBindToProjectCommand());
        regestry(new ProjectTaskUnbindToProjectCommand());

        regestry(new TaskShowCommand());
        regestry(new TaskChangeStatusByIdCommand());
        regestry(new TaskChangeStatusByIndexCommand());
        regestry(new TaskClearCommand());
        regestry(new TaskCompleteByIdCommand());
        regestry(new TaskCompleteByIndexCommand());
        regestry(new TaskCreateCommand());
        regestry(new TaskRemoveByIdCommand());
        regestry(new TaskRemoveByIndexCommand());
        regestry(new TaskShowByIdCommand());
        regestry(new TaskShowByIndexCommand());
        regestry(new TaskShowByProjectIdCommand());
        regestry(new TaskStartByIdCommand());
        regestry(new TaskStartByIndexCommand());
        regestry(new TaskUpdateByIdCommand());
        regestry(new TaskUpdateByIndexCommand());

        regestry(new UserLoginCommand());
        regestry(new UserLogoutCommand());
        regestry(new UserRegistryCommand());
        regestry(new UserChangePasswordCommand());
        regestry(new UserUpdateProfileCommand());
        regestry(new UserViewProfileCommand());
    }

}
